# 光年(Light Year Admin)后台管理系统模板

#### 演示网址
[http://lyear.itshubao.com](http://lyear.itshubao.com)

#### 介绍
![light year admin](https://images.gitee.com/uploads/images/2019/0314/224956_3eb2a29a_82992.png "未命名-1.png")

光年(Light Year Admin)后台管理系统模板是一个基于Bootstrap v3.3.7的纯HTML模板。

作为后端开发人员，自己在做一些简单系统时，经常为了后台的模板烦恼，国内的少，也不太喜欢tab形式的；国外的又太复杂；vue什么框架的又不会用，因而想自己整理出来一个简单点的通用后台模板，结合自己的使用和国外模板的配色、细节处理，这就有了光年后台模板。

简洁而清新的后台模板，功能虽少，倒也满足简单的后台功能，也能够快速上手，希望大家支持。

#### 特别感谢
- Bootstrap
- JQuery
- bootstrap-colorpicker
- bootstrap-datepicker
- bootstrap-datetimepicker
- ion-rangeslider
- jquery-confirm
- jquery-tags-input
- bootstrap-notify
- Chart.js
- chosen.jquery.js
- perfect-scrollbar

### 更新记录
2019.03.19 调整logo，修复滚动条bug

#### 登录页面
![光年模板登录页](https://images.gitee.com/uploads/images/2019/0316/223413_a840c9c2_82992.png "登录页面 - 光年(LightYear)后台管理系统模板.png")

#### 后台首页
![光年模板首页](https://images.gitee.com/uploads/images/2019/0314/231617_c0900993_82992.png "首页 - 光年(LightYear)后台管理系统模板.png")

#### 开关样式
![开关样式](https://images.gitee.com/uploads/images/2019/0316/224100_4a8494eb_82992.png "开关 - 光年(LightYear)后台管理系统模板.png")

#### 文档列表
![文档列表](https://images.gitee.com/uploads/images/2019/0316/223923_60231d3e_82992.png "文档列表 - 光年(LightYear)后台管理系统模板.png")